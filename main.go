// Copyright (c) 2018 Johannes Kohnen <gpl3-2017@ko-sys.com>
//
// This file is part of sqlping.
//
// sqlping is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// sqlping is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// sqlping.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"flag"
	"log"
	"time"

	"gitlab.com/wjkohnen/sqlping/pkg"
)

func main() {
	cfg := configure()
	if cfg.Driver != "postgres" && cfg.Driver != "mysql" {
		log.Fatalf("Unsupported driver: %q", cfg.Driver)
	}
	err := pkg.Ping(cfg)
	if err != nil {
		log.Fatalf("Error pinging %s: %v", cfg.Driver, err)
	}
}

func configure() pkg.Config {
	var c pkg.Config
	flag.StringVar(&c.Driver, "driver", "mysql", "driver {mysql,postgres}")
	flag.StringVar(&c.Host, "host", "localhost", "host")
	flag.IntVar(&c.Port, "port", 0, "port; 0 means 3306 on mysql, 5432 on postgres")
	flag.StringVar(&c.User, "user", "ping", "user")
	flag.StringVar(&c.Password, "password", "", "password")
	flag.DurationVar(&c.Timeout, "timeout", 3*time.Second, "timeout")
	flag.Parse()
	c.Port = getPort(c)
	return c
}

func getPort(cfg pkg.Config) int {
	var port int
	switch cfg.Driver {
	case "mysql":
		port = 3306
	case "postgres":
		port = 5432
	default:
		panic("invalid driver")
	}
	if cfg.Port != 0 {
		port = cfg.Port
	}
	return port
}
