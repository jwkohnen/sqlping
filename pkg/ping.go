// Copyright (c) 2018 Johannes Kohnen <gpl3-2017@ko-sys.com>
//
// This file is part of sqlping.
//
// sqlping is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// sqlping is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// sqlping.  If not, see <http://www.gnu.org/licenses/>.

package pkg

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq" // registers driver
)

type Config struct {
	Host     string
	Port     int
	User     string
	Password string
	Driver   string
	Timeout  time.Duration
}

func Ping(cfg Config) error {
	ctx := context.Background()
	var cancel func()
	if cfg.Timeout > 0 {
		ctx, cancel = context.WithTimeout(ctx, cfg.Timeout)
		defer cancel()
	}
	return pingContext(ctx, cfg)
}

func pingContext(ctx context.Context, cfg Config) error {
	connStr := getConnectionString(cfg)
	db, err := sql.Open(cfg.Driver, connStr)
	if err != nil {
		return fmt.Errorf("could not open db connection: %v", err)
	}
	defer func() {
		if e := db.Close(); e != nil {
			log.Printf("Error closing db connection: %v", e)
		}
	}()
	err = db.PingContext(ctx)
	if err != nil {
		return fmt.Errorf("could not ping db: %v", err)
	}

	return nil
}

func getConnectionString(cfg Config) string {
	var connStr string
	switch cfg.Driver {
	case "postgres":
		connStr = fmt.Sprintf("postgres://%s:%s@%s:%d/?sslmode=disable&connect_timeout=%d",
			cfg.User,
			cfg.Password,
			cfg.Host,
			cfg.Port,
			int(cfg.Timeout.Seconds()),
		)
	case "mysql":
		c := mysql.Config{
			User:         cfg.User,
			Passwd:       cfg.Password,
			Net:          "tcp",
			Addr:         cfg.Host + ":" + strconv.Itoa(cfg.Port),
			Timeout:      cfg.Timeout,
			ReadTimeout:  cfg.Timeout,
			WriteTimeout: cfg.Timeout,
		}
		connStr = c.FormatDSN()
	default:
		panic("invalid driver")
	}
	return connStr
}
